`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:27:40 03/26/2019 
// Design Name: 
// Module Name:    submodule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module submodule(
		clk_50khz,
		Locked,
		datalink,
		trig_in,
		status
    );

input clk_50khz,trig_in,Locked;
inout datalink;
output status;



endmodule
